package be.persgroep.adv.generic.automapper;

import be.persgroep.adv.generic.automapper.model.Person;
import be.persgroep.adv.generic.automapper.viewmodel.PersonViewModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: mcautreels
 * Date: 29/04/13
 * Time: 11:25
 */
public class PersonMapper extends AbstractViewModelMapper<Person, PersonViewModel> {
    @Override
    public PersonViewModel toViewModel(Person model) throws IllegalAccessException, InstantiationException {
        PersonViewModel personViewModel = (PersonViewModel) AbstractObjectMapper.convertObject(model, PersonViewModel.class);

        personViewModel.setSpamSafeEmail(model.getEmail().replace("@", "[at]"));

        return personViewModel;
    }

    @Override
    public Collection<PersonViewModel> toViewModels(Collection<Person> models) throws IllegalAccessException, InstantiationException {
        List<PersonViewModel> viewModels = new ArrayList<PersonViewModel>();

        for (Person model : models) {
            viewModels.add(toViewModel(model));
        }

        return viewModels;
    }

    @Override
    public Person toModel(PersonViewModel viewModel) throws IllegalAccessException, InstantiationException {
        return (Person) AbstractObjectMapper.convertObject(viewModel, Person.class);
    }

    @Override
    public Collection<Person> toModels(Collection<PersonViewModel> viewModels) throws IllegalAccessException, InstantiationException {
        return AbstractObjectMapper.convertObjectList(viewModels, Person.class);
    }
}
