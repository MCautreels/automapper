package be.persgroep.adv.generic.automapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: mcautreels
 */
public class AbstractObjectMapper {

    public static final String PACKAGE_VIEWMODEL = "be.persgroep.adv.rating.management.web.viewmodel";
    public static final String PACKAGE_MODEL = "be.persgroep.adv.rating.backend.model";

    public static Object convertObject(Object object, Class mapTo) throws InstantiationException, IllegalAccessException {
        if (object == null) {
            return null;
        }

        Object result = mapTo.newInstance();
        Method[] methods = mapTo.getMethods();

        for (Method method : methods) {
            if (isValidMethodName(method.getName())) {
                String fieldName = prepareFieldName(method.getName());
                Method[] valueMethods = object.getClass().getMethods();
                for (Method valueMethod : valueMethods) {
                    if (isValidValueMethodName(valueMethod.getName())) {
                        String valueFieldName = prepareValueFieldName(valueMethod.getName());
                        if (valueFieldName.equals(fieldName)) {
                            try {
                                Object value = valueMethod.invoke(object);

                                if (null == value) {
                                    break;
                                }

                                String packageName = value.getClass().getPackage().getName();
                                if (packageName.startsWith(PACKAGE_VIEWMODEL)
                                        || packageName.startsWith(PACKAGE_MODEL)) {
                                    break;
                                }

                                method.invoke(result, value);
                            } catch (InvocationTargetException e) {
                                // Do nothing for now!
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    private static String prepareValueFieldName(String name) {
        if (name.startsWith("get")) {
            return name.replace("get", "")
                    .replace("Jn", "");
        } else if (name.startsWith("is")) {
            return name.replace("is", "")
                    .replace("Jn", "");
        }
        return name;
    }

    private static boolean isValidValueMethodName(String name) {
        return name.startsWith("get") || name.startsWith("is");
    }

    private static String prepareFieldName(String name) {
        return name.replace("set", "")
                .replace("Jn", "");
    }

    private static boolean isValidMethodName(String name) {
        return name.contains("set");
    }

    public static List convertObjectList(Collection<?> objectList, Class mapTo) throws IllegalAccessException, InstantiationException {
        List<Object> resultList = new ArrayList<Object>();

        for (Object object : objectList) {
            resultList.add(convertObject(object, mapTo));
        }

        return resultList;
    }
}
