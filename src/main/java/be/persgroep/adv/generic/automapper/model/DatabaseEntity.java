package be.persgroep.adv.generic.automapper.model;

public abstract class DatabaseEntity {

    public abstract Long getId();
}